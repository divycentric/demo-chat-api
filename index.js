const express = require('express');
const socketio = require('socket.io');
const http = require('http');

const { addUser, removeUser, getUser, getUsersOfRoom } = require('./users')

const PORT = process.env.PORT || 8000;

const router = require('./router');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
const cors = require('cors');

// TODO : A better way to implement the events functionality is to use Event Sourcing design pattern with CQRS design pattern in conjunction.

const events = []

io.on('connection', (socket) => {
  socket.on('join', ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if (error)
      return callback(error);

    socket.join(user.room);

    //admin generated messages are called 'message'
    //welcome message for user

    socket.emit('message', { user: "admin", text: `${user.name}, welcome to the room ${user.room}` })
    events.push({ message: 'User Joined', type: 'USER_JOINED', timestamp: new Date() })
    //message to all the users of that room except the newly joined user
    socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} has joined` });


    io.to(user.room).emit('roomData', { room: user.room, users: getUsersOfRoom(user.room) })

    callback();
  })

  //user generated message are called 'sendMessage'
  socket.on('sendMessage', (message, callback) => {
    const user = getUser(socket.id);
    io.to(user.room).emit('message', { user: user.name, text: message, time: new Date() });
    io.to(user.room).emit('roomData', { room: user.room, users: getUsersOfRoom(user.room) });
    events.push({ message: 'Message', type: 'MESSAGE', timestamp: new Date() })
    callback();
  })

  // Send a high five to all users in room
  // TODO : Make a feature for users to be able to reply a particular user and hence do high five to them as well.
  socket.on('sendHighFive', async (message, callback) => {
    const user = await getUser(socket.id);
    io.to(user.room).emit('message', { user: user.name, text: `** ${user.name} did a high-five to everyone **` });
    io.to(user.room).emit('roomData', { room: user.room, users: getUsersOfRoom(user.room) });
    events.push({ message: 'High Five', type: 'HIGH_FIVE', timestamp: new Date() })
    callback()
  })

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);
    if (user) {
      io.to(user.room).emit('message', { user: 'admin', text: `${user.name} has left.` })
      events.push({ message: 'User left', type: 'USER_LEFT', timestamp: new Date() })
    }
  })
})

router.get('/filter-by-hours/:hours', (req, res) => {
  if (isNaN(req.params.hours)) {
    return res.status(400).send({ message: 'please enter a valid hour' })
  }
  if (req.params.hours > 5) {
    return res.status(400).send({ message: 'Please, dont enter an hour greater than 5' })
  }
  const filteredEvents = events.filter(event => new Date(event.timestamp).getHours() >= new Date().getHours() - req.params.hours)
  return res.status(200).send(filteredEvents)
})

app.use(cors());
app.use(router);


server.listen(PORT, () => {
  console.log(`Server Started on PORT ${PORT}`)
})
